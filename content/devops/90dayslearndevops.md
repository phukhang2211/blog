Title: Học kubernetes
Date: 2022-05-11
Slug: 90daysdevops
Authors: Khang Le
Summary: Nhật kí 90 ngày học devops

Số giờ đã học:
ngày 7/11: 1

Sau khi lượt 1 vòng thì mình hiểu kha khá về devops mà lười học Golang nên mình quay về học Ansible trước
xong Ansible trong 1 tuần thì sẽ dành trọn 1 ngày học Golang

Học Salt trước vậy, thầy mình dạy thế, học từ trang này https://pp.pymi.vn/article/salt/
chắc nhanh xong thôi, ráng tuần này 

các ý chính:
Salt hay salt stack là pm mã nguồn mở, Configuration Management. viết bằng python, sử dụng YAML là ngôn ngữ giao tiếp
với người dùng.
Tính năng chia làm 2 phần chính
Remote Execution: chạy lên từ xa, salt-master và salt-minion, từ master chạy câu lệnh trên máy minion


Configuration management
đảm bảo trạng thái các thành phần của hệ thống, vd cần đảm bảo nginx chạy với file cấu hình nhất định, forward request đến gunicorn app server trên máy, đã cấu hình để chạy web application viết bằng python với phiên bản mới nhất lấy từ respository?



- Đọc lại chào Muối em là ai lần nữa - đọc xong https://www.familug.org/2015/06/saltstack-chao-muoi-em-la-ai.html


mục tiêu:
chạy được salt cơ bản, xong sẽ học tiếp salt trên tài liệu trang chủ
45.76.146.105


Trên máy minion 
tạo user "sudo adduser saltuser", trả lời các câu hỏi và gõ password
sudo không password: thêm saltuser ALL=(ALL) NOPASSWD: ALL vào cuối file /etc/sudoers
Có chương trình sshd listen trên port 22, cài bằng lệnh sudo apt install openssh-server, gõ ss -nlt | grep 22 thấy có kết quả là ok.


cần cài ssh nếu chưa có, có rồi thì ko cần cài

Trên máy master

ls ~/.ssh/id_rsa || ssh-keygen

Copy ssh public key vào minion:

ssh-copy-id saltuser@192.168.0.110

rồi gõ password saltuser vừa tạo

Cài đặt
python3 -m venv saltenv
. saltenv/bin/activate
pip install salt==3002  # do bản 3003 mới nhất đang có bug


$ salt-ssh --version
salt-ssh 3002

cài salt-ssh bị lỗi ImportError: cannot import name 'Markup' from 'jinja2'
thì pip install jinja2==3.0.3


Cấu hình master
cần tạo 2 file: master và roster


tạo 1 thư mục saltlab:

mkdir -p ~/salt/saltlab/states
cd ~/salt/saltlab


file master tạo 2 dòng: root_dir là giá trị đường dẫn đầy đủ tới thư mục saltlab, file_roots có states với giá trị đầy đủ tới thu mục states, thư mục states sẽ chứa các file saltstate

root_dir: /home/khang/salt/saltlab
file_roots:
  base:
    -  /home/khang/salt/saltlab/states
pillar_roots:
  base:
    -  /home/khang/salt/saltlab/pillars


File roster chứa thông tin về các máy minions:

tv:
  host: 45.76.146.105
  user: saltuser
  sudo: True
trau:
  host: x.x.x.x
  user: root
  port: 22022

cấu trúc thư mục trong cả bài:

saltlab/
├── master
├── roster
├── pillars
│   ├── common.sls
│   ├── top.sls
│   └── uds.sls
└── states
    ├── example.sls
    ├── htop.sls
    ├── template.j2
    ├── uds.sls
    └── uds.systemd



chạy câu lệnh để run command từ máy master

salt-ssh --config ~/salt/saltlab tv cmnd.run 'uname -a'

tv:
    Linux bdss 5.4.0-131-generic #147-Ubuntu SMP Fri Oct 14 17:07:22 UTC 2022 x86_64 x86_64 x86_64 GNU/Linux


Apply Salt state:
Saltstate file là file YAML có đuôi .sls, khai báo (declare) trạng thái mong muốn hệ thống đạt được. htop.sls



install htop:   #  ID của "state"
  pkg.installed:  # loại state - tương ứng với 1 python function
    - name: htop   # các tham số - function argument


chạy câu lệnh:
salt-ssh --config ~/salt/saltlab tv state.apply htop


resutl:

tv:
----------
          ID: install htop
    Function: pkg.installed
        Name: htop
      Result: True
     Comment: All specified packages are already installed
     Started: 14:21:19.879589
    Duration: 60.139 ms
     Changes:   

Summary for tv
------------
Succeeded: 1
Failed:    0
------------
Total states run:     1
Total run time:  60.139 ms

mai học tiếp vậy nay ko khỏe, phải về ngủ thôi
day 1 gần xong rồi, hôm nay học khoảng 1 tiếng