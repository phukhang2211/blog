Title: Tài chính cá nhân
Date: 2023-01-27
Slug: taichinhcanhan1
Authors: phukhang2211
Summary: Tài chính cá nhân của mình

Minh có được cô giáo thân yêu giới thiệu cuốn thịnh vượng tài chính tuổi 30 từ sớm
khoảng sau khi tốt nghiệp đại học là bắt đầu đọc, cũng dần dần hình thành việc hiểu biết
và quản lý tài chính từ  sớm tuy nhiên đọc nhiều không có nghĩa là hiểu nhiều hay làm đúng
việc thật sự hiểu và bắt tay vào làm cho tài chính trở nên mạnh khỏe rất khác với việc chỉ biết
rồi để đó thôi.

Viết lại 1 sô điểm quan trọng mình rút ra từ quá trình đọc được và nghe postcad từ anh Hiếu Nguyễn và
bạn Thành Công TC, thì có nhiều giai đọan của 1 đời người cần trải qua về mặt tài chính và cách an toàn nhất
để tiến đến 1 tài chính có 1 sức khỏe tốt.

1. Trả nợ:
dĩ nhiên theo nhiều hoàn cảnh khác nhau mỗi người mỗi khác thì theo mình nợ là cái nên trả đầu tiên.
không nên có bất cứ khoảng nợ nào có lãi suất cả, cho dù nó rất bé, 
vd nợ thẻ tín dụng qua mỗi tháng hơn 3% ở mỗi ngân hàng, tính ra hơn 36% năm, trong khi lãi suất tiền gửi thông thường
tầm 6% năm, hay trong 2023 lạm phát cao cũng chỉ hơn 9%. Ngoài ra còn có nợ tiêu dùng, vay tín chập bọn này lãi thấp hơn 1 tí
nhưng để lại cũng ko tốt, vd 10-20% năm, vẫn rất cao hơn so với phần mình có thể tiết kiệm được.


2. Mua tài sản đảm bảo
phần này mình rút ra từ bản thân mình bán bảo hiểm manulife chứ không học được từ đâu cả.
mình đang có cho bản thân mình 3 bảo hiểm.
    1. điểm tựa đầu tư
    2. món quà tương lai
    3. phòng ngừa ung thư

nếu mình mất do ung tư hay tai nạn thì tất cả chi trả là hơn 3 tỷ.
cũng có mua cho gia đình, mẹ 1 bảo hiểm có đính kèm thẻ sức khỏe để phòng ngừa trường hợp cao tuổi
mà mình chưa có đủ tài chính để chữa trị, lúc đó cho dù có dư dc 1 chút hay có 1 ít tài sản thì cũng sẽ
phải bán di hết hay dùng hết tất cả phần mình có để chữa trị
mình cũng mua cho bé em gái 1 bảo hiểm giống mẹ, cái chính là để có chi phí nếu có bệnh ngoài khả năng chi tra của gia đình
ngoài phần sức khỏe ra thì nó còn có 1 phần để đầu tư, ví dụ hd của mẹ thì sau năm 65 hay 70t có thể rút ra 1 phần để coi như lương hưu, bé em thì sau này lớn lên phần của nó coi như được thêm 1 khoảng để chi tiêu trong cuộc sống.
hôm có mua cho ba 1 hợp đồng bảo hiểm nhưng tiếc quá sức khỏe của ba kém nên bảo hiểm từ chối, sáu tháng sau mới nộp lại xem sao. 

phần tài sản đảm bảo mình thấy cực kì cần thiết cho mỗi gia đình, nếu ai cũng có tài sản đảm bảo thì cho dù gia đình
có gặp phải bất trắc hay để lại tài sản cho thế hệ sau đều rất ổn.


3. Tích lũy - Đầu tư

Theo mình học được từ các anh thì khoản tích lũy sẽ bao gồm nhiều loại quĩ


quĩ khẩn cấp: 3-6 tháng chi tiêu, chỉ dùng cho việc khẩn cấp


quĩ an toàn: 3-6 tháng chi tiêu, có thể dùng để đóng tiền bảo hiểm có kế hoạch trước, hoặc có cơ hội đầu tư chắc ăn có thể dùng tạm rồi lắp đầy sau đó.


quĩ đầu tư: trích 5-50% hàng tháng cho dù còn nợ ít và đầu tư sớm nhất có thể. nếu chưa rành thì hãy học và tập đầu tư giá trị đừng nhìn vào lãi suất quá cao có thể rủi ro cao. thời gian ở trong thị trường thì tốt hơn là bên ngoài quan sát.
Đến khi nào có tài sản dc 20-25 năm chi tiêu là tôt nhất, dùng quy luật 4% để rút tiền thì có thể tự do tài chính đến già.


Hãy học nhiều nhất có thể trong khả năng của bạn.

phukhang2211

