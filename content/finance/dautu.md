Title: Bắt đầu đầu tư
Date: 2023-08-22
Slug: dautu
Authors: phukhang2211
Summary: Đầu tư

Mình viết note lại cũng như là hệ thống kiến thức về tài chính để làm tốt hơn trong việc quản lý tài chính của mình.
Muốn tham gia làm bất cứ thứ gì cũng cần phải có một lượng kiến thức tài chính nhất định bởi vì
giữ được tiền mình kiếm được thì quan trọng/khó hơn  là kiếm được nhiều tiền hơn ???
tùy thuộc vào hoàn cảnh và tính cách của mỗi người mà điều nay thay đổi, mình cũng hi vọng nhiều năm sau nếu thế hệ sau có đọc được
những dòng này hi vọng bạn sẽ có thể làm tốt hơn mình ở thời điểm gần 28t.
Giờ công nghệ AI tiến bộ nhiều quá, vài năm nữa không chừng sẽ có công cụ quản lý tài chính hết, con người có khi còn chẳng cần đụng tay.
Nhưng thôi cứ viết lại sau này biết đâu có cơ hội mình lại đọc được những dòng này.

Biết mình chi tiêu những gì mỗi ngày bằng cách ghi lại chi tiêu cực kì chi tiết, việc này chắc chắn sẽ không mất quá nhiều thời gian của bạn
mình khuyên nên dùng google sheet vì nó rất trực quan và khi ngồi lên máy tính bạn sẽ tập trung hơn nhiều so với cầm điện thoại chơi rồi thêm các mục
rất dễ bị xao lãng. nhưng nếu bạn thích dùng đt cũng có lựa chọn tốt, Money Lover app.
link này mình copy của bạn TC Thành Công, dùng dc 2 năm rồi, rất ok, bạn cũng có thể xem clip chi tiết hơn ở https://www.youtube.com/watch?v=qV7YvIOXpzY
https://drive.google.com/drive/folders/1Lr5Ukozq7J5gjWl1nylRAE__VT0bpgV3

Mình rất quan tâm về tài chính nên theo dõi nhiều kênh, nhưng có 3 kênh chính đặc biệt nhiều: Hieu Nguyen TV, Thành Công TC, Smart money VTV,
Quyển sách mình tâm đắc: Thịnh Vượng tài chính tuổi 30 tập 1 và 2 của Go Deuk Seong, Lập kế hoạch tài chính cá nhân của Kristy Shen, Bryce Leung.

Mình rút ra được 1 số bài học chung chung như sau trước khi mình sẽ đọc lại quyển sách và viết bài viết cực kì chi tiết hơn về việc review cho từng quyển sách.

Quy luật 4%, Theo như phần trên thì sau khoảng 3-6 tháng (ghi chép đầy đủ), thì mình sẽ có được khoản chi tiêu bắt buộc (cực kì cần thiết) mà mỗi tháng đều phải dùng, cố định
để mà sống tiếp cho dù có thu nhập hay không, theo đó ví dụ mỗi tháng mình có chi tiêu bắt buộc là 20tr/tháng, quy ra năm là 240tr/năm. lấy con số này nhân 25 lần là 6 tỷ.
Vậy 6 tỷ là con số được nhắm đển ở đây.
Tại sao là 6 tỷ? 
Nguyên tắc 4% có nhiều nguồn thống kê ở Mỹ, mỗi năm nếu người ta rút ra 4% danh mục đầu tư để chi trả chi phí sinh hoạt thì có 95% số lượng người rút sẽ có thể rút đến 30 năm sau.
đấy có thể coi như chìa khóa của việc nghỉ hưu sớm, hay tự do tài chính ở độ tuổi nào đó. hầu như ai có tìm hiểu về tài chính đều biết quy luật này, 
Vậy tại sao 95% thành công? 5% thất bại là tại sao?
Phần thất bài được gọi là rút lợi nhuận không đúng thời điểm (mình tự gọi nó, hmm trong sách văn vẻ lắm mà quên mất rồi - sẽ update sau), nghĩa là ngay khi bạn vừa nghỉ hưu, 
thì thị trường sập, mà phải mất đến 5 năm để phục hồi, nghĩa là lúc giá trị danh mục đầu tư đang thấp mà phải rút tiền cho chi tiêu thì danh mục sẽ giảm rất nhiều.
Còn nếu bạn vừa nghỉ hưu thì danh mục của bạn bước vào thời kì tăng trưởng, bạn sẽ có 10 năm siêu lợi nhuận.
Vấn đề là có cách nào để tránh việc thất bại này vì chúng ta không bao giờ biết trước được khi nào thị trường sẽ sập - CÓ đệm đỡ tiền mặt và khiên chắn lãi suất, phần này sẽ được
đề cập ở phân review về cuốn sách Lập kế hoạch tài chính cá nhân chi tiết hơn, nhưng 95% là khá ổn cho bạn ở mức độ chi tiết này rồi, thú vị chứ?

Vậy câu trả lời là tôi cần 6 tỷ, thế 6 tỷ ở đâu ra để tôi có 95% an toàn về mặt tài chính ở tuổi già?
Theo mình thì đầu tư là câu trả lời ở đây, không nhiều người có sẵn 6 tỷ lúc vừa mới sinh ra, nên chúng ta sẽ đi làm ở giai đoạn đầu của tuổi trẻ và tiết kiệm, đầu tư cho tương lai.
Bố mẹ nuôi nấng đến năm 18 tuổi rồi sau đó học đại học thêm 4 năm - bố mẹ đầu tư vào con cái khá là nhiều ở đây nhưng dứa nhỏ cần thêm 6 tỷ nữa. nó phải tự mình cố gắng.
Mình tin là động lực từ bên trong là quan trọng nhất, không nhiều bạn bè của mình ở độ tuổi này có kiến thức hay động lực để dành tiền, chi tiêu kỉ luật như mình - ít nhất là trong những người bạn mà mình quen biết. Đặc biết là người dân ở SG, miền tây quen với cách sống phóng khoáng, tiêu tiêu khá là nhiều và không kỉ luật.
Để đạt được mục tiêu 6 tỷ thì cách tốt nhất là bắt đầu đầu tư, tùy theo thu nhập/chi tiêu mà tiết kiệm 10-20% thu nhập, nếu thu nhập càng cao thì tỉ lệ đầu tư nên càng nhiều, dĩ nhiên là đầu tư vào các tài sản đảm bảo trước, như mô hình kim tự tháp, đáy càng vững chắc thì càng tốt, phải đó cũng là phần mà ít rủi ro, càng lên cao thì tỉ lệ sinh lời cao cũng như rủi ro cao.

Theo chương trình mình rất thích là Smart money, tập đầu tư từ đâu, mô hình kim tự tháp có 3 phần.
Phần đáy tháp giống như xây nhà, càng vũng chắc thì mình càng an tâm, do đó phần này là các quỹ dự phòng 6-12 tháng chi tiêu, quỹ khẩn cấp 3-6 tháng chi tiêu.
Bao gồm tiền mặt, tiền gửi, trái phiếu, vàng, bảo hiểm nhân thọ (sức khỏe), bảo hiểm y tế. Cần chuẩn bị hết các thứ này lúc còn trẻ sẽ mua được với giá tốt hơn.

Phần thân tháp thì sẽ là phần đầu tư với lãi suất cao hơn tăng trưởng cao hơn ví dụ như là đầu tư bất động sản, cho thuê nhà, đầu tư cổ phiếu an toàn/trái phiếu lợi suất cao.

Phần đỉnh tháp thì sẽ là rủi ro cao nên lợi nhuận cực cao ví dụ như mua bán nhà đất, cổ phiếu rủi ro, tiền mã hóa, mua bán ngoại tệ, forex ... phần này chỉ nên càng thấp càng tốt 1-5% tài sản. Để lỡ như có mất thì cũng không quá ảnh hưởng đến cuộc sống hiện tại, không bị tâm lý khi giao dịch.

Ngoài ra còn có 1 phần cực kì quan trọng đó là đầu tư và bản thân, ở độ tuổi từ 20-30 tuổi thì việc này cực kì cần thiết, có 1 ví dụ minh họa: 1 bạn sinh viên mới ra trường đi làm mức lương 10tr/tháng thì tài sản hiện có của bạn là bao nhiêu?

Câu trả lời của 1 anh giám đốc tài chính rất hay và thuyết phục mình, mức lương của bạn đó đang là 120tr/năm thì tương đương với 1 tài sản sẽ theo lãi suất 6%/năm để có 120tr thì tài sản của bạn đang là 2 tỷ (gửi 2 tỷ vào ngân hàng thì mỗi năm sẽ sinh lời 120tr)
Lời khuyên đầu tư cho bạn là nên trích ra 2tr/tháng để đầu tư hay trích ra 2tr/tháng để học tập nâng cao kĩ năng của bản thân lên cao hơn, ở độ tuổi này khả năng thu nhập được tăng cao là rất cao, ví dụ 1 năm sau khi bạn đầu tư 24tr vào chứng khoán lãi suất 10%/năm thì bạn được 24 + 2.4 = 26.4tr. theo mình phần này cũng rất tốt, sinh ra lãi kép từ sớm rất hiệu quả để có 1 tài sản kết xù sau nhiều năm. 
Nhưng cũng trường hợp nếu bạn làm việc xuất sắc và kĩ năng tăng cao thì mức lương bạn tăng lên 15tr/tháng, tức 180tr/năm thì tài sản của bạn hiện giờ đã tăng lên nhiều hơn rất nhiều, thành 3 tỷ. để 3 tỷ có được 180tr/năm theo lãi suất 6% như trên.
Nên lời khuyên của mình là cố gắng đầu tư càng sớm càng tốt và đầu tư cả vào bản thân nữa, càng có kỉ luật thì tỉ lệ thành công càng cao.
Có 1 câu mình rất tâm đắt là Time in market is better than timing market. Mình không nhớ rõ lắm nhưng chắc là ý chính vẫn đúng.

