Title: What is important in your life?
Date: 2022-08-11
Slug: what_is_important_in_your_life
Authors: phukhang2211
Summary: What is important in your life?

Cuộc sống vốn dĩ là rất ngắn ngủi
Vì thế cần cực kì cẩn thận lựa chọn những gì quan trọng nhất đối với mình,
dành thơi gian cho việc đó.
```
Sống hạnh phúc trọn vẹn từng phút giầy 
đừng làm cả ngày rồi tối mới sống, 
đừng làm cả tuần rồi cuối tuần mới sống,
đừng lam việc cả năm rồi tết mới sống.
```


Lê Hoàng Phú Khang aka phukhang2211


