Title: Nhìn lại, đúc kết 10 năm qua
Date: 2022-08-07
Slug: 10_years
Authors: phukhang2211
Summary: Nhìn lại, đúc kết 10 năm qua

Tự nhiên hôm nay lục lại được tấm hình 10 năm trước hồi học lớp 12.
Nghĩ về cuộc sống mình 10 năm qua đã được gì mà mất gì, còn điều gì hối tiếc chưa làm hay
điều gì hối tiếc là đã xảy ra.

```
Mất
```
Nói về mất mác trước, mất đi điểm tựa vững chải nhất trong cuộc đời của mình, rồi từ đó xảy ra nhiều chuyện 
không mong muốn, cũng từ đó cuộc đời mình sóng gió bủa vây, bắt đầu bương chải tìm cách tồn tại, rồi cũng may mắn
tìm được lớp học kỹ năng để rèn luyện bản thân, tìm được công việc tốt. 
Có lẽ đây là mất mác to lớn nhất những là điểm khởi đầu để mình bắt đầu trưởng thành hơn, cuộc sống có trách nhiệm và kết nối 
với gia đình hơn.
Cũng từ đó mất đi sự bình yên của 1 con gà công nghiệp được chăm sóc và nuôi nấng từ nhỏ, con gà công nghiệp trở thành gà rừng,
biết chạy uber, biết làm gia sư, biết đi múa kiếm tiền trang trải. May mắn con gà chưa bị bác thợ săn nào thịt nên tới giờ con gà
vẫn kiếm ăn và chăm lo cho gia đình được.
Năm 2022 cũng có 1 sự thay đổi là nhận ra được tìm đã tim ra 1 sự bình yên khác thay thế cái bình yên của con gà công nghiệp ngày xưa.
Bình yên trong tâm, có 1 câu rất hay mà gần đây mình rất thích:   `Tâm yên ngồi đâu cũng yên`.

```
Được
```
10 năm qua mất đi cũng nhiều thứ, những cũng có cái được, có kĩ năng sống, biết giao tiếp với mọi người tốt hơn, kĩ năng làm việc
cũng được nâng cao. có công việc tốt, đồng nghiệp tốt, bạn bè tốt, thầy cô tốt, sách tốt, tâm tốt, còn nhiều nhiều nữa.
Dĩ nhiên là không có gì hoàn hảo nhưng rồi thì mình cũng tìm được điểm tốt để chấp nhận và học hỏi.
Mừng thay con gà rừng đủ khả năng kiếm ăn cho nó và gia đình, có bản lĩnh và những người bạn luôn giúp đỡ để vuợt qua sống gió.
Mình cũng hi vọng đạt đến cái tầm có thể giúp người khác, hi vọng ai cũng có 1 cuộc sống tốt.
Hồi bé cũng nghĩ mình thông minh, dựa vào đó để thành công, nhưng lớn lên rồi mới nhận ra cái thông minh của mình nó bé tí, nhỏ xíu
chả đủ để kiếm ăn, cái mình thật sự cần là sự chăm chỉ, tập trung, tìm tòi, kiên nhẫn.
Dạo này đang có 1 thói quen tốt là ăn chay khi có thể, khá tốt cho sức khỏe và giảm cân hihi, đang dư cỡ chục kí mỡ thì ko tốt tí nào,
cũng bớt bia rượu cho cơ thể có thời gian phục hồi,chữa lành bản thân.


```
Tiếc nuối
```
Trải qua mất mác rồi mới nhận ra là lúc mình còn đầy đủ người thân, mình đã không làm tốt nhất có thể, để rồi có muốn quay lại 
1 lần nữa cũng sẽ là không bao giờ được, vì thế bây giờ và sau này mình luôn quí trọng từng người bên cạnh mình, làm tốt nhất có thể
để mọi người đều có cuộc sống tốt nhất, sau này không hối tiếc gì nữa. Rồi ai cũng sẽ tự hào vì mình, bản thân mình cũng tự hào về mình.


Điều hối tiếc mình đã làm là chưa định hướng rõ trước khi bước đi, nếu có 1 bản đồ trước khi đi thì chắc chắn sẽ ko đi lạc đường,
không phải đi đường vòng để đến mục tiêu, thời gian rất có hạn với tất cả mọi người, ai cũng chỉ có 24h 1 ngày, từ năm 18 tuổi trờ đi
chỉ có khoảng 40 năm để làm việc và phấn đấu, nếu không làm tốt, phần còn lại sẽ vô cùng khó khăn. nếu làm tốt, phần từ năm 60 tuổi trở đi
có thể sẽ làm điều mình yêu thích mà vẫn tạo được giá trị, chứ không phải lo cơm áo gạo tiền.

```
Tương lai
```
Từ ngày học pymi gặp thầy hvn thì mình cũng có niềm tin hơn về bản thân, biết bản thân có thể làm được, chỉ cần cố gắng sẽ có ngày thành công.
Cuộc đời mình từ đó thay đổi, biết có mục tiêu và cũng đạt dần được những mục tiêu đó.

Mình hôm nay gần chính xác chính là người mà mình muốn trở thành của 10 năm trước, cộng thêm nhiều điều mà mình chưa lường trước được, thêm nhiều hơn Đạo đức, nghị lực và 1 ít trí tuệ. Mình thấy cũng cần lên kế hoạch tiếp theo cho 5 năm sau, 10 năm, 15 năm, 20 năm và sau đó nữa.


Tự hứa với bản thân là sẽ viết nhiều hơn về mình để mình hiểu mình hơn và mọi chuyện cũng rõ ràng hơn. Tin tưởng vào bản thân mình hơn.

```
Không cần rất nhiều tiền để hạnh phúc, cần rất nhiều bản lĩnh để bình an.
```

Viết lúc 12h trưa ngày 7 tháng 8 năm 2022
tại TP.Hồ Chí Minh, nơi tuyệt vời nhất trên thế giới.

Lê Hoàng Phú Khang aka phukhang2211


Việc gì đã làm trong 10 năm qua (sai lầm, thất bại, thành tựu) => đừng mắc cùng 1 lỗi 2 lần
Việc gì muốn làm nhưng chưa làm được

Cần chi tiết hơn 10 năm qua nhỉ, thôi năm nay lỡ cỡ, 12 năm luôn

2010 năm này học lớp 10, học cũng khá giỏi, có cơ hội này nọ, có tình iu đầu đời :))) 
